$(document).ready(function () {
    $('header nav >div').click(function () {
        $('#sm-header').toggleClass('open');
        $('.bg-overlay').css('display','block');
    });
    $("#close-menu").click(function () {
        $('#sm-header').removeClass('open');
        $('.bg-overlay').css('display','none');
    });
    // form select vhandicap


    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
    var phone_number;
    $('.form-login form').submit(function (e) {
        e.preventDefault();
        var urlCheckLogin;
        phone_number = $(this).find('input').val();
        console.log(phone_number);
        $.ajax({
            url: urlCheckLogin,
            data: {phone_number: phone_number},
            success: function (result) {
                $(this).parent().addClass('hidden');
                // neu da co tai khoan, mo form nhap otp
                $(this).parent().siblings('.form-otp').removeClass('hidden');
                // chua co tai khoan
                $(this).parent().siblings('.form-register').removeClass('hidden');
            }
        });
        $(this).parent().addClass('hidden');
        // neu da co tai khoan
        $(this).parent().siblings('.form-otp').removeClass('hidden');

    });
    var $form_otp = $('.form-otp');
    $('.form-otp form').submit(function (e) {
        e.preventDefault();
        var url_otp;
        var otp = $(this).find('input').val();
        $.ajax({
            url: url_otp,
            data: {otp: otp},
            success: function (result) {
                // neu dang nhap thanh cong

                //  neu dang nhap fail
                $form_otp.find('.text-danger').text('** Mã OTP của bạn không đúng');
                $form_otp.find('button').addClass('hidden');
                $form_otp.find('#resend').removeClass('hidden');
                $form_otp.find('input').val('').addClass('hidden');
            }
        });
        $('#exampleModal').modal('toggle');

    });

    $('#resend').click(function () {
        $('.form-login form').submit();
        $form_otp.find('.text-danger').text('');
        $form_otp.find('button').removeClass('hidden');
        $form_otp.find('input').removeClass('hidden');
        $form_otp.find('#resend').addClass('hidden');
    });


    //custom select type
    $("body").on("click", ".select", function(){
        $(this).toggleClass('open');
    });
    $("body").on("click", ".select ul li",function (event) {
        event.stopPropagation();
        var select = $(this).text();
        var $parent = $(this).closest('.select');
        $parent.removeClass('open');
        $parent.find('input').val(select);
        changeValue($parent.find('input').attr('id'),select);
    });
});