const list = [
    {name: '', phone: '', course: '', vhandicap: '', size: ''},
];
var source   = document.getElementById("flight-template").innerHTML;
var template = Handlebars.compile(source);
function addGolfer(flight, group, data) {
    var id = "flight_" + flight + "_" + group;
    var name = 'flight[' + flight + ']' + '[' + group + ']';
    var context = {id: id, name: name, group: data};
    var html    = template(context);
    return html;
    // template =
    //     '<div>' +
    //     '    <div class="line"></div>' +
    //     '    <div class="group col-xl-8" id="'+id+'">' +
    //     '        <div>' +
    //     '            <div class="row align-items-center">' +
    //     '                <div class="col-lg-4 title">Họ tên<span>*</span></div>' +
    //     '                <div class="col-lg-8 no-padding">' +
    //     '                    <input type="text" id="'+id+'_name" name="'+name+'[name]" value="'+data.name+'">' +
    //     '                </div>' +
    //     '            </div>' +
    //     '            <div class="row align-items-center">' +
    //     '                <div class="col-lg-4 title">Số điện thoại<span>*</span></div>' +
    //     '                <div class="col-lg-8 no-padding">' +
    //     '                    <input type="text" id="'+id+'_phone" name="'+name+'[phone]" value="'+data.phone+'">' +
    //     '                </div>' +
    //     '            </div>' +
    //     '            <div class="row align-items-center">' +
    //     '                <div class="col-lg-4 title">VHandicap<span>*</span></div>' +
    //     '                <div class="col-lg-8 no-padding d-md-flex">' +
    //     '                    <div class="select">' +
    //     '                        <input type="text" readonly="" placeholder="VHandicap" id="'+id+'_course" name="'+name+'[course]" value="'+data.course+'">' +
    //     '                        <ul>' +
    //     '                            <li>San Long Thanh</li>' +
    //     '                            <li>San Dong Mo</li>' +
    //     '                            <li>San Long Thanh</li>' +
    //     '                        </ul>' +
    //     '                    </div>' +
    //     '                    <input class="vhandicap" type="text" id="'+id+'_vhandicap" name="'+name+'[vhandicap]" value="'+data.vhandicap+'">' +
    //     '                </div>' +
    //     '            </div>' +
    //     '            <div class="row align-items-center">' +
    //     '                <div class="col-lg-4 title">Size áo</div>' +
    //     '                <div class="col-lg-8 no-padding">' +
    //     '                    <div class="select">' +
    //     '                        <input type="text" readonly="" placeholder="S" id="'+id+'_size" name="'+name+'[size]" value="'+data.size+'">' +
    //     '                        <ul>' +
    //     '                            <li>S</li>' +
    //     '                            <li>M</li>' +
    //     '                            <li>L</li>' +
    //     '                            <li>XL</li>' +
    //     '                        </ul>' +
    //     '                    </div>' +
    //     '                </div>' +
    //     '            </div>' +
    //     '            <div class="row align-items-center">' +
    //     '                <div class="col-lg-4 title">Lệ phí</div>' +
    //     '                <div class="col-lg-8 no-padding main-color">2.500.000</div>' +
    //     '            </div>' +
    //     '            <div class="second-button remove">Xóa</div>' +
    //     '            <div class="move d-flex flex-column justify-content-center">' +
    //     '                <i class="fa fa-arrow-up"></i><i class="fa fa-arrow-down"></i>' +
    //     '            </div>' +
    //     '        </div>' +
    //     '    </div>' +
    //     '</div>';
    // return template;
    
}
function changePosition(a,b) {
    var arrayA = a.split("_");
    var arrayB = b.split("_");
    var indexA = Number(arrayA[1])*4 + Number(arrayA[2]);
    var indexB = Number(arrayB[1])*4 + Number(arrayB[2]);
    elementA = list[indexA];
    list[indexA] = list[indexB];
    list[indexB] = elementA;
    renderFlight();
}
function deleteGroup(id){
    var array = id.split("_");
    var index = Number(array[1])*4 + Number(array[2]);
    list.splice(index,1);
    renderFlight();
    if (list.length==1){
        $("#wrap").removeClass('show');
    }
}
function changeValue(id,val){
    var array = id.split("_");
    var index = Number(array[1])*4 + Number(array[2]);
    list[index][array[3]] = val;
    
}
function renderFlight() {
    var flights = [];
    $.each( list, function( k, v ){
        if(k%4 == 0){
            flights.push([v]);
        } else {
            flights[Math.floor(k/4)].push(v);
        }
    });
    var tem = '';
    $.each(flights, function(k1,groups){
        var groupTem = '';
        $.each(groups,function(k2,group){
            groupTem+=addGolfer(k1,k2,group);
        });
        tem+='<div class="list-group">'+'<div class="main-color">Flight '+Number(k1+1)+'</div>'+groupTem+'</div>';
    
    });
    $("#wrap").html(tem);
}
renderFlight();
$("#add_user").click(function(){
    list.push({name: '', phone: '', course: '', vhandicap: '', size: ''});
    renderFlight();
    if (list.length>1){
        $("#wrap").addClass('show');
    }
});

var dragElement;
var dropElement;
$(document).on("mouseenter", ".group", function () {
    var $this = $(this);
    $(this).draggable({
        axis: "y",
        handle: ".move",
        stack: ".group",
        revert: function (e) {
            if (dropElement != undefined) {
                $("#" + dropElement).css("top", 0 + "px");
                $("#" + dragElement).css("top", 0 + "px");
                changePosition(dragElement, dropElement);
                dropElement = undefined;
                return false
            } else {
                return true
            }
        },
        start: function (e) {
            dragElement = e.target.id;
        }
    });

});

$(document).on("mouseover ", ".group", function () {
    $(this).droppable({
        accept: ".group",
        drop: function (e) {
            dropElement = e.target.id
            
        }
    });
});

$("body").on("click", ".remove", function(){
    deleteGroup($(this).closest('.group').attr('id'));
});
$("body").on("keyup change", "input", function(){
    changeValue($(this).attr('id'),$(this).val());
});
